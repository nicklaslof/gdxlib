I found myself keeping on reinventing the wheel every prototype or in every game jam. So instead of doing that I can use this little framework lib to do all the common things I use LibGDX for.

It probably makes no sense for other developers and there are probably another lib/framework like this but this one fits my workflow and how I use LibGDX.