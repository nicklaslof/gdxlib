package st.rhapsody.gdxlib.text;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Align;
import st.rhapsody.gdxlib.debug.DebugPrint;

public class FontDrawer {

    private static GlyphLayout layout;
    private static BitmapFont font;

    private static FontDrawer instance = new FontDrawer();


    public static FontDrawer getInstance() {
        return instance;
    }

    public FontDrawer() {
        layout = new GlyphLayout();
    }

    public void setFont(BitmapFont font){
        this.font = font;
    }

    public void drawTextAt(SpriteBatch spriteBatch, String text, int x, int y, boolean wrap){
        drawTextAt(spriteBatch, text, x, y, wrap, Align.left, 1.0f);
    }

    public void drawTextAt(SpriteBatch spriteBatch, String text, int x, int y, boolean wrap, int align){
        drawTextAt(spriteBatch, text, x, y, wrap, align, 1.0f);
    }

    public void drawTextAt(SpriteBatch spriteBatch, String text, int x, int y, boolean wrap, int align, float scale){
        if (font == null){
            font = new BitmapFont();
            DebugPrint.print(this, "Initalized with default font",0);
        }
        text = text.toUpperCase();
        font.setUseIntegerPositions(false);
        font.getData().setLineHeight(20f);
        font.getData().setScale(scale);

        if (wrap) {
            GlyphLayout draw = font.draw(spriteBatch, text, x, y, 450, align, true);

            font.draw(spriteBatch, draw, x, y);
        }else{
            layout.setText(font, text);
            font.draw(spriteBatch,layout,x,y);
        }

    }

    public void dispose() {
        font.dispose();
    }
}
