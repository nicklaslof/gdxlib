package st.rhapsody.gdxlib.physic;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;

import java.util.List;

import static st.rhapsody.gdxlib.physic.Box2dConvert.toBox;

public class PhysicBuilder {

    private static PhysicBuilder instance = new PhysicBuilder();

    private World world;


    public static PhysicBuilder getInstance() {
        return instance;
    }

    public void setWorld(World world) {
        this.world = world;
    }

    public Body build(PhysicTemplate physicsTemplate) {

        Body body = null;
        switch(physicsTemplate.getType()){
            case CIRCLE:
                body = buildCircle(physicsTemplate);
                break;
            case RECTANGLE:
                body = buildRectangle(physicsTemplate);
                break;
        }

        if (physicsTemplate.hasRangeSensor()){
            addRangeSensor(body, physicsTemplate);
        }

        return body;
    }

    private void addRangeSensor(Body body, PhysicTemplate physicsTemplate) {
        PolygonShape polygonShape = new PolygonShape();
        polygonShape.setAsBox(toBox(physicsTemplate.getRangeSensorWidth()), toBox(physicsTemplate.getRangeSensorHeight())
                , new Vector2(
                        toBox(
                                (physicsTemplate.getHeight()/2)+physicsTemplate.getOriginOffsetX()),
                        toBox((physicsTemplate.getWidth()/2)+physicsTemplate.getOriginOffsetY())), 0f);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.isSensor = true;
        fixtureDef.filter.categoryBits = physicsTemplate.getRangeSensorCategoryFilter();
        fixtureDef.filter.maskBits = physicsTemplate.getRangeSensorMaskFilter();
        fixtureDef.restitution = 0;
        fixtureDef.friction = 0;
        fixtureDef.density = 0;
        fixtureDef.shape = polygonShape;
        Fixture fixture = body.createFixture(fixtureDef);
        fixture.setUserData(new RangeSensor(physicsTemplate.getRangeSensorUserData()));

    }

    private Body buildRectangle(PhysicTemplate physicsTemplate) {
        Body body = getBody(physicsTemplate);

        PolygonShape polygonShape = new PolygonShape();

        polygonShape.setAsBox(toBox(physicsTemplate.getWidth()), toBox(physicsTemplate.getHeight())
                , new Vector2(
                        toBox(
                                (physicsTemplate.getHeight()/2)+physicsTemplate.getOriginOffsetX()),
                        toBox((physicsTemplate.getWidth()/2)+physicsTemplate.getOriginOffsetY())), 0f);
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.isSensor = physicsTemplate.hasSensor();
        fixtureDef.density = physicsTemplate.getDensity();
        fixtureDef.friction = physicsTemplate.getFriction();
        fixtureDef.restitution = physicsTemplate.getRestitution();
        fixtureDef.shape = polygonShape;
        fixtureDef.filter.categoryBits = physicsTemplate.getCategoryFilter();
        fixtureDef.filter.maskBits = physicsTemplate.getMaskFilter();
        body.createFixture(fixtureDef);

        return body;
    }

    private Body buildCircle(PhysicTemplate physicsTemplate) {
        Body body = getBody(physicsTemplate);

        CircleShape circleShape = new CircleShape();
        circleShape.setRadius(toBox(physicsTemplate.getRadius()));
        circleShape.setPosition(new Vector2(toBox(physicsTemplate.getRadius()), toBox(physicsTemplate.getRadius())));
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = circleShape;
        fixtureDef.density = physicsTemplate.getDensity();
        fixtureDef.friction = physicsTemplate.getFriction();
        fixtureDef.restitution = physicsTemplate.getRestitution();
        fixtureDef.filter.categoryBits = physicsTemplate.getCategoryFilter();
        fixtureDef.filter.maskBits = physicsTemplate.getMaskFilter();

        if (physicsTemplate.hasSensor()){
            fixtureDef.isSensor = true;
        }

        body.createFixture(fixtureDef);

        return body;
    }

    private Body getBody(PhysicTemplate physicsTemplate) {
        BodyDef bodyDef = createBodyDef(physicsTemplate);
        bodyDef.fixedRotation = physicsTemplate.hasFixedRotation();
        bodyDef.gravityScale = physicsTemplate.getGravityScale();
        Body body = world.createBody(bodyDef);
        return body;
    }

    private BodyDef createBodyDef(PhysicTemplate physicsTemplate) {
        BodyDef bodyDef = new BodyDef();
        if (physicsTemplate.isKinematic()) {
            bodyDef.type = BodyDef.BodyType.KinematicBody;
        }else if (physicsTemplate.isStatic()){
            bodyDef.type = BodyDef.BodyType.StaticBody;
        }else{
            bodyDef.type = BodyDef.BodyType.DynamicBody;
        }
        return bodyDef;
    }

    public void dispose(Body body) {
        world.destroyBody(body);
    }


    //Build without template
    public Body build(BodyDef bodyDef, List<FixtureDefPair> fixtureDefList) {
        Body body = world.createBody(bodyDef);

        for (FixtureDefPair fixtureDefPair : fixtureDefList) {
            Fixture fixture = body.createFixture(fixtureDefPair.getFixtureDef());
            fixture.setUserData(fixtureDefPair.getUserData());
        }

        return body;
    }
}
