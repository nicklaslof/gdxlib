package st.rhapsody.gdxlib.physic;

public class RangeSensor {

    private Object userData;

    public RangeSensor(Object userData) {
        this.userData = userData;
    }

    public Object getUserData() {
        return userData;
    }
}
