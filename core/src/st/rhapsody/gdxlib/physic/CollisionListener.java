package st.rhapsody.gdxlib.physic;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;

public interface CollisionListener {

    public void onCollisionBegin(Fixture myCollidedFixture, Fixture collidedFixture, Body collidedBody);
    public void onCollisionEnd(Fixture myCollidedFixture, Fixture collidedFixture, Body collidedBody);
}
