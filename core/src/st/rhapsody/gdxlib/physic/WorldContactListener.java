package st.rhapsody.gdxlib.physic;

import com.badlogic.gdx.physics.box2d.*;
import st.rhapsody.gdxlib.gameobject.GameObject;

public class WorldContactListener implements ContactListener {
    @Override
    public void beginContact(Contact contact) {
        Fixture fixtureA = contact.getFixtureA();
        Fixture fixtureB = contact.getFixtureB();

        Object userDataA = fixtureA.getBody().getUserData();
        Object userDataB = fixtureB.getBody().getUserData();

        if (userDataA instanceof GameObject){
            ((GameObject)userDataA).onCollisionBegin(fixtureA, fixtureB, fixtureB.getBody());
        }

        if (userDataB instanceof GameObject){
            ((GameObject)userDataB).onCollisionBegin(fixtureB, fixtureA, fixtureA.getBody());
        }

    }

    @Override
    public void endContact(Contact contact) {
        Fixture fixtureA = contact.getFixtureA();
        Fixture fixtureB = contact.getFixtureB();

        Object userDataA = fixtureA.getBody().getUserData();
        Object userDataB = fixtureB.getBody().getUserData();

        if (userDataA instanceof GameObject){
            ((GameObject)userDataA).onCollisionEnd(fixtureA, fixtureB, fixtureB.getBody());
        }

        if (userDataB instanceof GameObject){
            ((GameObject)userDataB).onCollisionEnd(fixtureB, fixtureA, fixtureA.getBody());
        }
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }
}
