package st.rhapsody.gdxlib.physic;

import com.badlogic.gdx.physics.box2d.FixtureDef;

public class FixtureDefPair {
    private FixtureDef fixtureDef;
    private Object userData;

    public FixtureDefPair(FixtureDef fixtureDef, Object userData) {
        this.fixtureDef = fixtureDef;
        this.userData = userData;
    }

    public FixtureDefPair(FixtureDef fixtureDef) {
        this.fixtureDef = fixtureDef;
        this.userData = null;
    }

    public FixtureDef getFixtureDef() {
        return fixtureDef;
    }

    public Object getUserData() {
        return userData;
    }
}
