package st.rhapsody.gdxlib.physic;

public class PhysicTemplate {
    private short categoryFilter;
    private short maskFilter;
    private float rangeSensorWidth;
    private float rangeSensorHeight;
    private short rangeSensorCategoryFilter;
    private short rangeSensorMaskFilter;
    private Object rangeSensorUserData;

    public Object getRangeSensorUserData() {
        return rangeSensorUserData;
    }


    public enum Type{
        RECTANGLE,
        CIRCLE
    }

    private Type type;
    private boolean kinematic;
    private boolean staticBody;
    private boolean sensor;
    private float radius;
    private int width;
    private int height;
    private float originOffsetX;
    private float originOffsetY;
    private float density = 1.0f;
    private float friction = 0.0f;
    private float restitution = 0.0f;
    private boolean fixedRotation = true;
    private boolean rangeSensor = false;
    private float gravityScale = 1.0f;

    public boolean isKinematic() {
        return kinematic;
    }

    public boolean isStatic() {
        return staticBody;
    }

    public boolean hasSensor() {
        return sensor;
    }

    public PhysicTemplate asSensor(){
        this.sensor = true;
        return this;
    }

    public float getRadius() {
        return radius;
    }

    public PhysicTemplate withWidth(int width) {
        this.width = width;
        return this;
    }

    public PhysicTemplate withHeight(int height) {
        this.height = height;
        return this;
    }

    public PhysicTemplate withOriginOffsetX(float x){
        this.originOffsetX = x;
        return this;
    }

    public PhysicTemplate withOriginOffsetY(float y){
        this.originOffsetY = y;
        return this;
    }

    public PhysicTemplate withFixedRotation(boolean b){
        this.fixedRotation = b;
        return this;
    }

    public PhysicTemplate withRangeSensor(float width, float height, short sensorIsA, short sensorCollidesWith, Object userData){
        this.rangeSensor = true;
        this.rangeSensorWidth = width;
        this.rangeSensorHeight = height;
        this.rangeSensorCategoryFilter = sensorIsA;
        this.rangeSensorMaskFilter = sensorCollidesWith;
        this.rangeSensorUserData = userData;
        return this;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public float getOriginOffsetX() {
        return originOffsetX;
    }

    public float getOriginOffsetY() {
        return originOffsetY;
    }

    public float getDensity() {
        return density;
    }

    public float getFriction() {
        return friction;
    }

    public float getRestitution() {
        return restitution;
    }

    public boolean hasFixedRotation() {
        return fixedRotation;
    }

    public float getRangeSensorHeight() {
        return rangeSensorHeight;
    }

    public float getRangeSensorWidth() {
        return rangeSensorWidth;
    }

    public float getGravityScale() {
        return gravityScale;
    }

    public boolean hasRangeSensor() {
        return rangeSensor;
    }

    public PhysicTemplate withDensity(float density) {
        this.density = density;
        return this;
    }

    public PhysicTemplate withFriction(float friction) {
        this.friction = friction;
        return this;
    }

    public PhysicTemplate withRestitution(float restitution) {
        this.restitution = restitution;
        return this;
    }

    public PhysicTemplate withGravityScale(float gravityScale){
        this.gravityScale = gravityScale;
        return this;
    }

    public PhysicTemplate filterIsA(short categoryFilter) {
        this.categoryFilter = categoryFilter;
        return this;
    }

    public PhysicTemplate filterCollidesWith(short maskFilter) {
        this.maskFilter = maskFilter;
        return this;
    }

    public PhysicTemplate asKinematic(){
        this.kinematic = true;
        return this;
    }

    public PhysicTemplate asStatic(){
        this.staticBody = true;
        return this;
    }

    public PhysicTemplate asType(Type type){
        this.type = type;
        return this;
    }

    public Type getType() {
        return type;
    }

    public PhysicTemplate withRadius(float radius){
        this.radius = radius;
        return this;
    }


    public short getCategoryFilter() {
        return categoryFilter;
    }

    public short getMaskFilter() {
        return maskFilter;
    }

    public short getRangeSensorCategoryFilter() {
        return rangeSensorCategoryFilter;
    }

    public short getRangeSensorMaskFilter() {
        return rangeSensorMaskFilter;
    }
}
