package st.rhapsody.gdxlib.gameobject;

import com.badlogic.gdx.utils.Pool;
import st.rhapsody.gdxlib.debug.DebugPrint;

public class GameObjectPool extends Pool<GameObject> {

    public GameObjectPool(int initialCapacity, int max) {
        super(initialCapacity, max);
    }

    @Override
    protected GameObject newObject() {
        return new GameObject();
    }

    @Override
    public void free(GameObject gameObject) {
        gameObject.clear();
        super.free(gameObject);
    }

    @Override
    public GameObject obtain() {
        GameObject gameObject = super.obtain();
        gameObject.clear();
        return gameObject;
    }
}
