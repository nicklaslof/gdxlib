package st.rhapsody.gdxlib.gameobject;

import com.badlogic.gdx.utils.Array;
import st.rhapsody.gdxlib.debug.DebugPrint;
import st.rhapsody.gdxlib.render.Render;

import java.util.ArrayList;

public class GameObjects {
    private static GameObjects instance = new GameObjects();
    private ArrayList<GameObject> newgameObjects = new ArrayList<GameObject>();
    private ArrayList<GameObject> gameObjects = new ArrayList<GameObject>();
    private ArrayList<GameObject> disposedGameObjects = new ArrayList<GameObject>();


    public static GameObjects getInstance() {
        return instance;
    }

    public void addGameObject(GameObject gameObject){
        newgameObjects.add(gameObject);
    }

    public void tick(float delta){

        gameObjects.addAll(newgameObjects);
        newgameObjects.clear();

        for (GameObject gameObject : gameObjects) {
            if (gameObject.isEnabled()) {
                gameObject.tick(delta);
            }
            if (gameObject.isDisposed()){
                disposedGameObjects.add(gameObject);
            }
        }

        if (!disposedGameObjects.isEmpty()){
            //DebugPrint.print(this, "Gameobjects to dispose: "+disposedGameObjects.size()+" of "+gameObjects.size(),0);
            for (GameObject disposedGameObject : disposedGameObjects) {
                //DebugPrint.print(this, "Disposing: "+disposedGameObject, 60);
                disposedGameObject.dispose();
            }
            gameObjects.removeAll(disposedGameObjects);
            disposedGameObjects.clear();
        }

    }

    public void render(Render render){
        for (GameObject gameObject : gameObjects) {
            if (gameObject.isEnabled()) {
                gameObject.render(render);
            }
        }
    }

    public void dispose() {

        //DebugPrint.print(this, "Gameobjects to dispose: "+gameObjects.size()+" of "+gameObjects.size(),0);
        for (GameObject gameObject : gameObjects) {
            gameObject.dispose();
        }

        gameObjects.clear();

       // DebugPrint.print(this, "newgameObjects to dispose: "+newgameObjects.size()+" of "+newgameObjects.size(),0);
        for (GameObject gameObject : newgameObjects) {
            gameObject.dispose();
        }

        newgameObjects.clear();

       // DebugPrint.print(this, "disposedGameObjects to dispose: "+disposedGameObjects.size()+" of "+disposedGameObjects.size(),0);
        for (GameObject gameObject : disposedGameObjects) {
            gameObject.dispose();
        }

        disposedGameObjects.clear();
    }
}
