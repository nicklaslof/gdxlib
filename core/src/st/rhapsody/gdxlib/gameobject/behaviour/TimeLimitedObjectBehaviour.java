package st.rhapsody.gdxlib.gameobject.behaviour;

import st.rhapsody.gdxlib.gameobject.BaseBehaviour;
import st.rhapsody.gdxlib.gameobject.GameObject;

public class TimeLimitedObjectBehaviour extends BaseBehaviour {
    @Override
    public void tick(float delta, GameObject gameObject) {
        super.tick(delta, gameObject);
        counters.decreaseAllCounters(this, delta);

        if (counters.isCounterZeroOrBelow(this, "ttl")){
            gameObject.setDisposed();
        }
    }

    public BaseBehaviour withTTL(float ttl){
        counters.setCounter(this, "ttl", ttl);
        return this;
    }
}
