package st.rhapsody.gdxlib.gameobject;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import st.rhapsody.gdxlib.effects.Light;
import st.rhapsody.gdxlib.physic.CollisionListener;
import st.rhapsody.gdxlib.physic.FixtureDefPair;
import st.rhapsody.gdxlib.physic.PhysicBuilder;
import st.rhapsody.gdxlib.physic.PhysicTemplate;
import st.rhapsody.gdxlib.render.AnimatedRenderBehavior;
import st.rhapsody.gdxlib.render.Render;
import st.rhapsody.gdxlib.render.RenderBehaviour;

import java.util.List;

import static st.rhapsody.gdxlib.physic.Box2dConvert.*;

public class GameObject implements Pool.Poolable {

    private Vector2 position = new Vector2();
    private float scale = 1.0f;
    private float rotation = 0.0f;
    private Color tintColor = new Color(Color.WHITE);
    private Color originalTintColor = new Color(Color.WHITE);

    private Array<GameObjectSprite> sprites = new Array<GameObjectSprite>();

    private Array<BaseBehaviour> behaviours = new Array<BaseBehaviour>();
    private Array<RenderBehaviour> renderBehaviours = new Array<RenderBehaviour>();

    private Array<Light> lights = new Array<Light>();

    private boolean disposed;
    private Body body;

    private static GameObjectPool gameObjectPool = new GameObjectPool(1000,50000);
    private int state;
    private boolean flippedLeft;
    private boolean upSideDown;
    private boolean enabled = true;
    private CollisionListener onCollisionListener;


    GameObject() {

    }

    /**
     * Gets a GameObject from the pool. Don't forget to add it to GameObjects or it will not get any ticks.
     *
     * Call setDisposed when finished using it so it can be reused!
     * @return
     */
    public static GameObject obtain() {
        return gameObjectPool.obtain();
    }

    /**
     * Gets a GameObject from the pool and adds it to GameObjects
     *
     * Call setDisposed when finished using it so it can be reused!
     * @return
     */

    public static GameObject obtainAndAdd() {
        GameObject gameObject = gameObjectPool.obtain();
        GameObjects.getInstance().addGameObject(gameObject);
        return gameObject;
    }

    public void tick(float delta) {
        if (body != null){
            this.position.set(toWorld(body.getPosition().x), toWorld(body.getPosition().y));
            this.rotation = toWorld(body.getAngle());
        }
        if (behaviours.size == 0){
            this.behaviours.add(new BaseBehaviour());
        }
        for (BaseBehaviour baseBehaviour : behaviours) {
            baseBehaviour.tick(delta, this);
        }

        for (RenderBehaviour renderBehaviour : renderBehaviours) {
            if (renderBehaviour instanceof AnimatedRenderBehavior){
                ((AnimatedRenderBehavior)renderBehaviour).setCurrentStateId(state);
            }
            renderBehaviour.tick(this, delta);
        }

        for (Light light : lights) {
            light.transform(position.x, position.y);
        }
    }

    public void render(Render render){
        if (renderBehaviours.size == 0){
            this.renderBehaviours.add(new RenderBehaviour());
        }
        for (RenderBehaviour renderBehaviour : renderBehaviours) {
            renderBehaviour.render(this, render);
        }
    }

    public void onCollisionBegin(Fixture myCollidedFixture, Fixture collidedFixture, Body collidedBody) {
        if (this.onCollisionListener != null){
            this.onCollisionListener.onCollisionBegin(myCollidedFixture, collidedFixture, collidedBody);
        }
    }

    public void onCollisionEnd(Fixture myCollidedFixture, Fixture collidedFixture, Body collidedBody) {
        if (this.onCollisionListener != null){
            this.onCollisionListener.onCollisionEnd(myCollidedFixture, collidedFixture, collidedBody);
        }
    }

    public Array<GameObjectSprite> getEntitySprites() {
        return this.sprites;
    }

    public Body getBody() {
        return body;
    }

    //************************************************************************
    // All transformation methods
    //************************************************************************

    public Vector2 getPosition() {
        return this.position;
    }

    public float getRotation(){
        return this.rotation;
    }


    public void translate(float x, float y){
        if (body == null) {
            this.position.add(x, y);
        }else{
            body.applyForceToCenter(toBox(x), toBox(y),true);
        }
    }

    public void setScale(float scale){
        this.scale = scale;
    }

    public void rotate(float rotate){
        if (body != null){
            //TODO: not sure it's the correct method
            body.applyTorque(rotate,true);
        }else {
            this.rotation += rotate;
        }
    }

    public void scale(float scale){
        this.scale += scale;
    }

    public void setTintColor(Color color){
        this.tintColor.set(color);
    }

    public void resetTintColor(){
        this.tintColor.set(this.originalTintColor);
    }

    public float getScale() {
        return scale;
    }

    public Color getTintColor() {
        return tintColor.cpy();
    }
    //************************************************************************
    // Handle disposal of an GameObject
    //************************************************************************


    public void setDisposed() {
        this.disposed = true;
    }

    public boolean isDisposed() {
        return disposed;
    }

    public void dispose() {

        for (BaseBehaviour baseBehaviour : behaviours) {
            baseBehaviour.dispose();
        }
        for (RenderBehaviour renderBehaviour : renderBehaviours) {
            renderBehaviour.dispose();
        }

        for (Light light : lights) {
            light.setDisposed();
        }

        if (body != null) {
            PhysicBuilder.getInstance().dispose(body);
        }

        gameObjectPool.free(this);

        //DebugPrint.print(gameObjectPool, "Free:" + gameObjectPool.getFree()+ "  Peak:"+gameObjectPool.peak+"  Max:"+gameObjectPool.max,10);
    }

    public void disposeBody(){
        if (body != null) {
            PhysicBuilder.getInstance().dispose(body);
        }

        body = null;
    }

    public void setDisabled(){
        this.enabled = false;
    }

    public void setEnabled(){
        this.enabled = true;
    }

    public boolean isEnabled() {
        return enabled;
    }

    //************************************************************************
    // On creation setters
    //************************************************************************

    public GameObject addSprite(Sprite sprite, Render.ZLayer ZLayer){
        this.sprites.add(new GameObjectSprite(sprite, ZLayer));
        return this;
    }

    public GameObject onPosition(float x, float y){
        this.position.set(x, y);
        if (body != null){
            this.body.setTransform(toBox(position.x), toBox(position.y), 0f);
        }
        return this;
    }

    /**
     * Defaults to 1.0f if not set.
     * @param scale
     * @return
     */
    public GameObject withScale(float scale){
        this.scale = scale;
        return this;
    }

    /**
     * Defaults to White if not set.
     * @param color
     * @return
     */
    public GameObject withTintColor(Color color){
        this.tintColor.set(color);
        this.originalTintColor.set(color);
        return this;
    }

    public GameObject addBehaviour(BaseBehaviour baseBehaviour){
        this.behaviours.add(baseBehaviour);
        return this;
    }

    public GameObject addRenderBehaviour(RenderBehaviour behaviour){
        this.renderBehaviours.add(behaviour);
        return this;
    }

    public GameObject addLight(Light light){
        this.lights.add(light);
        return this;
    }
    public GameObject withPhysicsTemplate(PhysicTemplate physicsTemplate){
        return withPhysicsTemplate(physicsTemplate, null);
    }

    public GameObject withPhysicsTemplate(PhysicTemplate physicsTemplate, Object userData){
        this.body = PhysicBuilder.getInstance().build(physicsTemplate);
        if (userData != null){
            this.body.setUserData(userData);
        }else {
            this.body.setUserData(this);
        }
        this.body.setTransform(toBox(position.x), toBox(position.y), 0f);
        return this;
    }

    public GameObject withPhysics(BodyDef bodyDef, List<FixtureDefPair> fixtureDefPairList){
        return withPhysics(bodyDef, fixtureDefPairList, null);
    }

    public GameObject withPhysics(BodyDef bodyDef, List<FixtureDefPair> fixtureDefPairsList, Object bodyUserData){
        this.body = PhysicBuilder.getInstance().build(bodyDef, fixtureDefPairsList);
        if (bodyUserData != null){
            this.body.setUserData(bodyUserData);
        }else {
            this.body.setUserData(this);
        }
        this.body.setTransform(toBox(position.x), toBox(position.y), 0f);
        return this;
    }

    public GameObject withCollisionListener(CollisionListener collisionListener){
        this.onCollisionListener = collisionListener;
        return this;
    }

    public void clear() {
        this.position.setZero();
        this.scale = 1.0f;
        this.rotation = 0.0f;
        this.lights.clear();
        this.behaviours.clear();
        this.renderBehaviours.clear();
        this.sprites.clear();
        this.tintColor.set(Color.WHITE);
        this.originalTintColor.set(Color.WHITE);
        this.body = null;
        this.disposed = false;
        this.state = 0;
        this.flippedLeft = false;
        this.upSideDown = false;
        this.enabled = true;
    }

    //************************************************************************

    public void setState(int id){
        this.state = id;
    }

    public void stop(){
        if (this.body != null){
            body.setLinearVelocity(0,0);
        }
    }

    public void flipLeft() {
        this.flippedLeft = true;
    }
    public void flipRight() {
        this.flippedLeft = false;
    }

    public void flipUpSideDown() {
        this.upSideDown = true;
    }

    public void flipNormal() {
        this.upSideDown = false;
    }

    public boolean isFlippedLeft() {
        return flippedLeft;
    }

    public boolean isUpSideDown() {
        return upSideDown;
    }

    @Override
    public void reset() {
        clear();
    }

    public static class GameObjectSprite {
        private Render.ZLayer ZLayer;
        private Sprite sprite;

        public GameObjectSprite(Sprite sprite, Render.ZLayer ZLayer) {
            this.ZLayer = ZLayer;
            this.sprite = sprite;
        }

        public Render.ZLayer getZLayer() {
            return ZLayer;
        }

        public Sprite getSprite() {
            return sprite;
        }
    }

    /*@Override
    public String toString() {
        return "GameObject{" +
                "position=" + position +
                ", scale=" + scale +
                ", rotation=" + rotation +
                ", tintColor=" + tintColor +
                ", originalTintColor=" + originalTintColor +
                ", sprites=" + sprites +
                ", behaviours=" + behaviours +
                ", renderBehaviours=" + renderBehaviours +
                ", lights=" + lights +
                ", disposed=" + disposed +
                ", body=" + body +
                ", state=" + state +
                ", flippedLeft=" + flippedLeft +
                ", upSideDown=" + upSideDown +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GameObject that = (GameObject) o;

        if (Float.compare(that.scale, scale) != 0) return false;
        if (Float.compare(that.rotation, rotation) != 0) return false;
        if (disposed != that.disposed) return false;
        if (state != that.state) return false;
        if (flippedLeft != that.flippedLeft) return false;
        if (upSideDown != that.upSideDown) return false;
        if (position != null ? !position.equals(that.position) : that.position != null) return false;
        if (tintColor != null ? !tintColor.equals(that.tintColor) : that.tintColor != null) return false;
        if (originalTintColor != null ? !originalTintColor.equals(that.originalTintColor) : that.originalTintColor != null)
            return false;
        if (sprites != null ? !sprites.equals(that.sprites) : that.sprites != null) return false;
        if (behaviours != null ? !behaviours.equals(that.behaviours) : that.behaviours != null) return false;
        if (renderBehaviours != null ? !renderBehaviours.equals(that.renderBehaviours) : that.renderBehaviours != null)
            return false;
        if (lights != null ? !lights.equals(that.lights) : that.lights != null) return false;
        return body != null ? body.equals(that.body) : that.body == null;
    }

    @Override
    public int hashCode() {
        int result = position != null ? position.hashCode() : 0;
        result = 31 * result + (scale != +0.0f ? Float.floatToIntBits(scale) : 0);
        result = 31 * result + (rotation != +0.0f ? Float.floatToIntBits(rotation) : 0);
        result = 31 * result + (tintColor != null ? tintColor.hashCode() : 0);
        result = 31 * result + (originalTintColor != null ? originalTintColor.hashCode() : 0);
        result = 31 * result + (sprites != null ? sprites.hashCode() : 0);
        result = 31 * result + (behaviours != null ? behaviours.hashCode() : 0);
        result = 31 * result + (renderBehaviours != null ? renderBehaviours.hashCode() : 0);
        result = 31 * result + (lights != null ? lights.hashCode() : 0);
        result = 31 * result + (disposed ? 1 : 0);
        result = 31 * result + (body != null ? body.hashCode() : 0);
        result = 31 * result + state;
        result = 31 * result + (flippedLeft ? 1 : 0);
        result = 31 * result + (upSideDown ? 1 : 0);
        return result;
    }*/
}
