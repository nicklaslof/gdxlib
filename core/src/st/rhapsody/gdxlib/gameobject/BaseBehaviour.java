package st.rhapsody.gdxlib.gameobject;

import st.rhapsody.gdxlib.utils.Counter;

public class BaseBehaviour {
    protected final Counter counters = Counter.getInstance();
    public void tick(float delta, GameObject gameObject) {

    }

    public void dispose() {
        counters.disposeObject(this);
    }
}
