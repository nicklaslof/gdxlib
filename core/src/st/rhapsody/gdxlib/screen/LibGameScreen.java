package st.rhapsody.gdxlib.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.World;
import st.rhapsody.gdxlib.effects.Lights;
import st.rhapsody.gdxlib.gameobject.GameObjects;
import st.rhapsody.gdxlib.input.InputHandler;
import st.rhapsody.gdxlib.physic.Box2dConvert;
import st.rhapsody.gdxlib.physic.PhysicBuilder;
import st.rhapsody.gdxlib.physic.WorldContactListener;
import st.rhapsody.gdxlib.render.Render;

public abstract class LibGameScreen implements Screen{
    private final int viewportWidth;
    private final int viewPortHeight;
    private final int uiViewportWidth;
    private final int uiViewPortHeight;
    private final float zoom;
    private float screenR;
    private float screenG;
    private float screenB;
    private float screenA;

    private float shadowR = 1.0f;
    private float shadowG = 1.0f;
    private float shadowB = 1.0f;
    private float shadowA = 1.0f;


    private FrameBuffer lightBuffer;
    private SpriteBatch finalLight;
    private SpriteBatch lightSpriteBatch;

    private FPSLogger fpsLogger = new FPSLogger();

    protected OrthographicCamera mainCamera;
    private SpriteBatch mainSpriteBatch;
    private OrthographicCamera uiCamera;
    private SpriteBatch uiSpriteBatch;

    private Box2DDebugRenderer debugRenderer;
    private Matrix4 debugCameraMatrix;

    // TODO: Move to correct class later
    private World world;


    public LibGameScreen(int viewportWidth, int viewPortHeight, float zoom) {
        this(viewportWidth, viewPortHeight, viewportWidth, viewPortHeight, zoom);
    }

    public LibGameScreen(int mainViewportWidth, int mainViewPortHeight, int uiViewportWidth, int uiViewPortHeight, float zoom) {
        this.viewportWidth = mainViewportWidth;
        this.viewPortHeight = mainViewPortHeight;
        this.uiViewportWidth = uiViewportWidth;
        this.uiViewPortHeight = uiViewPortHeight;
        this.zoom = zoom;
    }

    @Override
    public void show() {
        mainCamera = new OrthographicCamera(viewportWidth,viewPortHeight);
        mainCamera.zoom = zoom;
        mainCamera.position.set(0,0,0);
        mainCamera.update();

        uiCamera = new OrthographicCamera(uiViewportWidth,uiViewPortHeight);
        uiCamera.position.set(0,0,0);
        uiCamera.update();

        lightBuffer = new FrameBuffer(Pixmap.Format.RGBA8888, viewportWidth, viewPortHeight, false);
        lightBuffer.getColorBufferTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);


        mainSpriteBatch = new SpriteBatch();
        uiSpriteBatch = new SpriteBatch();
        finalLight = new SpriteBatch();
        lightSpriteBatch = new SpriteBatch();

        // TODO: Move to correct class later
        World world = physicsWorld();
        if (world != null){
            this.world = world;
            ContactListener contactListener = getContactListener();
            if (contactListener != null){
                this.world.setContactListener(contactListener);
            }else{
                this.world.setContactListener(new WorldContactListener());
            }
        }

        debugRenderer = new Box2DDebugRenderer();
        PhysicBuilder.getInstance().setWorld(this.world);

        Gdx.input.setInputProcessor(InputHandler.getInstance());

        if (cursorCatched()){
            Gdx.input.setCursorCatched(true);
            Gdx.input.setCursorPosition(Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()/2);
        }
    }

    @Override
    public void render(float delta) {
        Render.getInstance().reset();
        Gdx.gl.glClearColor(screenR, screenG, screenB, screenA);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        tickAll(delta);

        if (showCrossHair()){
            InputHandler.getInstance().handleCrossHairScreenLimit();
        }

        renderMain();

        renderLight();

        mergeLights();

        renderUI();

        renderCrossHair();

        renderDebugPhysic();

        if (fpsLoggerEnabled()) {
            fpsLogger.log();
        }
    }

    private void renderDebugPhysic() {
        if (debugRenderPhysic()) {
            debugCameraMatrix = new Matrix4(mainCamera.combined);

            debugCameraMatrix.translate(mainCamera.position.x / 1024, mainCamera.position.y / 1024, 0f);
            debugCameraMatrix.scale(Box2dConvert.BOX_TO_WORLD, Box2dConvert.BOX_TO_WORLD, 0);

            //TODO: move to right class later
            debugRenderer.render(world,debugCameraMatrix);
        }
    }

    private void tickAll(float delta) {
        GameObjects.getInstance().tick(delta);
        Lights.getInstance().tick(delta);
        // TODO: Move to correct class later
        if (world != null) {
            world.step(1f / 60f, 8, 3);
        }
        tick(delta);
    }

    private void renderCrossHair() {
        mainSpriteBatch.begin();
        renderCrossHair(mainSpriteBatch);
        mainSpriteBatch.end();
    }

    private void renderUI() {
        uiSpriteBatch.begin();
        uiSpriteBatch.setProjectionMatrix(uiCamera.combined);
        renderUI(uiSpriteBatch);
        uiSpriteBatch.end();
    }

    private void renderMain() {
        mainSpriteBatch.begin();
        mainSpriteBatch.setProjectionMatrix(mainCamera.combined);
        mainSpriteBatch.enableBlending();
        renderBeforeGameObjects(mainSpriteBatch);
        mainSpriteBatch.end();

        mainSpriteBatch.begin();
        mainSpriteBatch.setProjectionMatrix(mainCamera.combined);
        mainSpriteBatch.enableBlending();
        GameObjects.getInstance().render(Render.getInstance());
        Render.getInstance().render(mainSpriteBatch);
        mainSpriteBatch.end();

        mainSpriteBatch.begin();
        mainSpriteBatch.setProjectionMatrix(mainCamera.combined);
        mainSpriteBatch.enableBlending();
        renderAfterGameObjects(mainSpriteBatch);
        mainSpriteBatch.end();
    }

    private void renderCrossHair(SpriteBatch uiSpriteBatch) {
        if (showCrossHair() && crossHairSprite() != null){
            Vector3 crosshairPosition = getUnprojectedCrosshairLocation();
            Sprite crosshair = crossHairSprite();
            crosshair.setOriginBasedPosition(crosshairPosition.x, crosshairPosition.y);
            crosshair.draw(uiSpriteBatch);
        }
    }

    public Vector3 getUnprojectedCrosshairLocation() {
        return InputHandler.getInstance().getUnprojectedCrosshairPosition(mainCamera);
    }

    public Vector3 getProjectedCrosshairLocation() {
        return InputHandler.getInstance().getProjectedCrosshairPosition(mainCamera);
    }

    private void renderLight() {
        lightBuffer.begin();
        Gdx.gl.glClearColor(shadowR, shadowG, shadowB, shadowA);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        lightSpriteBatch.enableBlending();
        lightSpriteBatch.begin();
        lightSpriteBatch.setProjectionMatrix(mainCamera.combined);

        lightSpriteBatch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE);

        Lights.getInstance().render(lightSpriteBatch);

        lightSpriteBatch.end();
        lightBuffer.end();
    }

    private void mergeLights() {
        finalLight.enableBlending();
        finalLight.setBlendFunction(GL20.GL_DST_COLOR, GL20.GL_ZERO);
        finalLight.begin();
        finalLight.setProjectionMatrix(lightSpriteBatch.getProjectionMatrix().idt());
        finalLight.draw(lightBuffer.getColorBufferTexture(), -1, 1, 2, -2);
        finalLight.end();
        finalLight.disableBlending();
    }

    public void setScreenBackgroundColor(float screenR, float screenG, float screenB, float screenA){

        this.screenR = screenR;
        this.screenG = screenG;
        this.screenB = screenB;
        this.screenA = screenA;
    }

    public void setScreenShadowColor(float shadowR, float shadowG, float shadowB, float shadowA){

        this.shadowR = shadowR;
        this.shadowG = shadowG;
        this.shadowB = shadowB;
        this.shadowA = shadowA;
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        mainSpriteBatch.dispose();
        uiSpriteBatch.dispose();
        GameObjects.getInstance().dispose();
        Lights.getInstance().dispose();
    }

    public abstract Sprite crossHairSprite();
    public abstract boolean showCrossHair();
    public abstract boolean cursorCatched();
    public abstract boolean fpsLoggerEnabled();
    public abstract boolean debugRenderPhysic();
    public abstract World physicsWorld();
    public abstract ContactListener getContactListener();
    protected abstract void tick(float delta);
    protected abstract void renderUI(SpriteBatch uiSpriteBatch);
    protected abstract void renderBeforeGameObjects(SpriteBatch mainSpriteBatch);
    protected abstract void renderAfterGameObjects(SpriteBatch mainSpriteBatch);
}
