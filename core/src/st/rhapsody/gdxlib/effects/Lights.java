package st.rhapsody.gdxlib.effects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import st.rhapsody.gdxlib.effects.behaviour.LightBehaviour;

public class Lights {

    private static Lights instance = new Lights();
    private Array<Light> lights = new Array<Light>();
    private Array<Light> disposedLights = new Array<Light>();

    public static Lights getInstance() {
        return instance;
    }


    public Light addLight(float posX, float posY, Color color, Sprite sprite, float scale, LightBehaviour lightBehaviour){
        Light light = new Light()
                .onPosition(posX, posY)
                .withColor(color)
                .withSprite(sprite)
                .withScale(scale)
                .withLightBehaviour(lightBehaviour);

        lights.add(light);
        return light;
    }

    public void tick(float delta){
        for (Light light : lights) {
            light.tick(delta);
            if (light.isDisposed()){
                disposedLights.add(light);
                light.dispose();
            }
        }


        if (disposedLights.size > 0) {
            lights.removeAll(disposedLights, true);
            disposedLights.clear();
        }

    }

    public void render(SpriteBatch spriteBatch){
        for (Light light : lights) {
            light.render(spriteBatch);
        }
    }

    public void dispose() {
        lights.clear();
    }
}
