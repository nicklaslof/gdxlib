package st.rhapsody.gdxlib.effects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import st.rhapsody.gdxlib.effects.behaviour.LightBehaviour;

public class Light {
    private Vector2 position = new Vector2();
    private Color color = new Color(Color.WHITE);
    private Sprite lightSprite;
    private LightBehaviour lightBehaviour = new LightBehaviour();
    private float scale;

    private boolean defaultLocation = true;

    private boolean disposed;

    Light() {
    }

    public Light onPosition(float x, float y){
        defaultLocation = false;
        this.position.set(x, y);
        return this;
    }

    public Light withColor(Color color){
        this.color.set(color);
        return this;
    }

    public Light withSprite(Sprite sprite){
        this.lightSprite = sprite;
        return this;
    }

    public Light withScale(float scale){
        this.scale = scale;
        return this;
    }

    public Light withLightBehaviour(LightBehaviour lightBehaviour){
        this.lightBehaviour = lightBehaviour;
        return this;
    }

    public void transform(float x, float y){
        defaultLocation = false;
        this.position.set(x, y);
    }

    public void setDisposed() {
        this.disposed = true;
    }

    public Sprite getSprite() {
        return lightSprite;
    }

    public float getScale() {
        return scale;
    }

    public boolean isDisposed() {
        return disposed;
    }

    public LightBehaviour getLightBehaviour() {
        return lightBehaviour;
    }

    public Vector2 getPosition() {
        return position;
    }

    public void tick(float delta) {
        this.lightBehaviour.tick(delta, this);
    }

    public void render(SpriteBatch spriteBatch) {
        if (defaultLocation){
            return;
        }
        this.lightBehaviour.render(this);
        lightSprite.draw(spriteBatch);
    }

    public Color getColor() {
        return color;
    }

    public void dispose() {
        this.lightBehaviour.dispose();
    }

    public void setScale(float scale) {
        this.scale = scale;
    }

    public void scale(float scale){
        this.scale += scale;
    }

    public void setColor(Color color) {
        this.color.set(color);
    }
}
