package st.rhapsody.gdxlib.effects.behaviour;

import com.badlogic.gdx.graphics.Color;
import st.rhapsody.gdxlib.effects.Light;
import st.rhapsody.gdxlib.utils.Counter;

public class LightExplosionBehaviour extends LightBehaviour{

    public static final Counter c = Counter.getInstance();

    public LightExplosionBehaviour() {

    }

    public LightBehaviour withTTL(float ttl){
        Counter.getInstance().setCounter(this,"ttl", ttl);
        return this;
    }

    @Override
    public void tick(float delta, Light light) {
        super.tick(delta, light);
        c.decreaseCounter(this,"ttl", delta);

        float counter = c.getCounter(this, "ttl");
        light.setScale(counter * 10.0f);
        Color color = light.getColor();
        float colorSub = counter * 0.4f;

        color.sub(colorSub,colorSub,colorSub,colorSub);
        light.setColor(color);

        if (c.isCounterZeroOrBelow(this, "ttl")){
            light.setDisposed();
        }
    }
}
