package st.rhapsody.gdxlib.effects.behaviour;

import com.badlogic.gdx.graphics.g2d.Sprite;
import st.rhapsody.gdxlib.effects.Light;
import st.rhapsody.gdxlib.utils.Counter;

public class LightBehaviour {

    private int offsetX;
    private int offsetY;

    public void tick(float delta, Light light){

    }

    public LightBehaviour withOffset(int offsetX, int offsetY) {
        this.offsetX = offsetX;
        this.offsetY = offsetY;
        return this;
    }


    public void render(Light light){
        Sprite sprite = light.getSprite();
        sprite.setScale(light.getScale());
        sprite.setColor(light.getColor());
        sprite.setOriginBasedPosition(light.getPosition().x + offsetX, light.getPosition().y + offsetY);
        //DebugPrint.print(this, light.getPosition(), 60);
    }

    public void dispose(){
        Counter.getInstance().disposeObject(this);
    }
}
