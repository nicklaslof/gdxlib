package st.rhapsody.gdxlib.utils;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.TextureData;

public class ImageToPixelReader {

    public interface PixelCallback{
        public void onPixel(int x, int y, int color);
    }


    /**
     * Used to read in an image and return the color and x and y position
     * for generating tiled levels from an image
     * @param texture
     * @param pixelCallback
     */
    public void readPixels(Texture texture, PixelCallback pixelCallback){

        TextureData textureData = texture.getTextureData();

        if (!textureData.isPrepared()){
            textureData.prepare();
        }

        Pixmap pixels = textureData.consumePixmap();

        for (int x = 0; x < texture.getWidth(); x++) {
            for (int y = 0; y < texture.getHeight(); y++) {
                int pixel = pixels.getPixel(x, y);
                int tileY = (texture.getHeight() - 1) - y; // Reverse Y since libGdx has Y starting at the bottom of the screen and not at the top.

                pixelCallback.onPixel(x, tileY, pixel);
            }
        }

    }


}
