package st.rhapsody.gdxlib.utils;

import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.ObjectMap;

import java.util.HashMap;
import java.util.Map;

public class Counter {
    private static Counter instance = new Counter();
    private Map<Object, ArrayMap<String, CounterObject>> counters = new HashMap<Object, ArrayMap<String, CounterObject>>();

    public static Counter getInstance() {
        return instance;
    }

    public float decreaseCounter(Object object, String name, float n) {
        ArrayMap<String, CounterObject> objectCounters = safeGetCounter(object, name);
        CounterObject counter = objectCounters.get(name);
        counter.decrease(n);
        return counter.getCounter();
    }

    public float increaseCounter(Object object, String name, float n) {
        ArrayMap<String, CounterObject> objectCounters = safeGetCounter(object, name);
        CounterObject counter = objectCounters.get(name);
        counter.increase(n);
        return counter.getCounter();
    }

    public void decreaseAllCounters(Object object, float n){
        ArrayMap<String, CounterObject> allCounters = safeGetAllCounters(object);
        for (ObjectMap.Entry<String, CounterObject> entry : allCounters) {
            entry.value.decrease(n);
        }
    }

    private ArrayMap<String, CounterObject> safeGetAllCounters(Object object) {
        if (!counters.containsKey(object)) {
            counters.put(object, new ArrayMap<String, CounterObject>());
        }

        return counters.get(object);
    }

    public void setCounter(Object object, String name, float n) {
        ArrayMap<String, CounterObject> objectCounters = safeGetCounter(object, name);
        objectCounters.get(name).setCounter(n);

    }

    public float getCounter(Object object, String name) {
        ArrayMap<String, CounterObject> objectCounters = safeGetCounter(object, name);
        return objectCounters.get(name).getCounter();
    }

    public void disposeCounter(Object object, String name){
        if (counters.containsKey(object)){
            ArrayMap<String, CounterObject> entries = counters.get(object);
            if (entries.containsKey(name)){
                entries.removeKey(name);
            }
        }
    }

    public void disposeObject(Object object){
        if (counters.containsKey(object)){
            counters.remove(object);
        }
    }

    private ArrayMap<String, CounterObject> safeGetCounter(Object object, String name) {
        if (!counters.containsKey(object)) {
            counters.put(object, new ArrayMap<String, CounterObject>());
        }

        ArrayMap<String, CounterObject> objectCounters = counters.get(object);

        if (!objectCounters.containsKey(name)) {
            objectCounters.put(name, new CounterObject());
        }
        return objectCounters;
    }



    public boolean isCounterZeroOrBelow(Object object, String name) {
        if (hasCounter(object, name)) {
            ArrayMap<String, CounterObject> entries = safeGetCounter(object, name);
            if (entries.get(name).getCounter() <= 0) {
                return true;
            }
        }
        return false;
    }

    public boolean hasCounter(Object object, String name) {
        if (!counters.containsKey(object) || !counters.get(object).containsKey(name)){
            return false;
        }

        return true;
    }

    private class CounterObject{
        private float n = 0.0f;

        void increase(float n){
            this.n += n;
        }

        void decrease(float n){
            this.n -= n;
        }

        float getCounter(){
            return n;
        }

        public void setCounter(float n) {
            this.n = n;
        }
    }

}
