package st.rhapsody.gdxlib.input;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector3;

public class InputHandler implements InputProcessor {

    private static InputHandler instance = new InputHandler();
    private static Vector3 crossHairTmp = new Vector3();
    private boolean leftPressed;
    private boolean rightPressed;
    private boolean upPressed;
    private boolean downPressed;
    private boolean spacePressed;
    private boolean spaceReleased;
    private boolean touchDown;
    private boolean touchReleased;


    public static InputHandler getInstance() {
        return instance;
    }

    @Override
    public boolean keyDown(int keycode) {
        boolean handled = false;
        switch (keycode){
            case Input.Keys.A:
            case Input.Keys.LEFT:
                handled = this.leftPressed = true;
                break;
            case Input.Keys.D:
            case Input.Keys.RIGHT:
                handled = this.rightPressed = true;
                break;
            case Input.Keys.W:
            case Input.Keys.UP:
                handled = this.upPressed = true;
                break;
            case Input.Keys.S:
            case Input.Keys.DOWN:
                handled = this.downPressed = true;
                break;
            case Input.Keys.SPACE:
                handled = this.spacePressed = true;
                this.spaceReleased = false;
                break;
        }

        return handled;
    }

    @Override
    public boolean keyUp(int keycode) {
        boolean handled = false;
        switch (keycode){
            case Input.Keys.A:
            case Input.Keys.LEFT:
                handled = this.leftPressed = false;
                break;
            case Input.Keys.D:
            case Input.Keys.RIGHT:
                handled = this.rightPressed = false;
                break;
            case Input.Keys.W:
            case Input.Keys.UP:
                handled = this.upPressed = false;
                break;
            case Input.Keys.S:
            case Input.Keys.DOWN:
                handled = this.downPressed = false;
                break;
            case Input.Keys.SPACE:
                handled = this.spacePressed = false;
                this.spaceReleased = true;
                break;
        }

        return handled;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        this.touchDown = true;
        this.touchReleased = false;
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        this.touchDown = false;
        this.touchReleased = true;
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    public boolean isLeftPressed() {
        return leftPressed;
    }

    public boolean isRightPressed() {
        return rightPressed;
    }

    public boolean isUpPressed() {
        return upPressed;
    }

    public boolean isDownPressed() {
        return downPressed;
    }

    public boolean isSpacePressed() {
        return spacePressed;
    }

    public boolean isSpaceReleased() {
        boolean returnValue = spaceReleased;
        spaceReleased = false;
        return returnValue;
    }

    public boolean isTouchDown() {
        return touchDown;
    }

    public boolean isTouchReleased() {
        boolean returnValue = touchReleased;
        touchReleased = false;
        return returnValue;
    }

    public Vector3 getUnprojectedCrosshairPosition(OrthographicCamera camera){
        crossHairTmp.set(Gdx.input.getX(), Gdx.input.getY(), 0);
        Vector3 unproject = camera.unproject(crossHairTmp);
        return unproject;
    }

    public Vector3 getProjectedCrosshairPosition(OrthographicCamera camera) {
        crossHairTmp.set(Gdx.input.getX(), Gdx.input.getY(), 0);
        Vector3 project = camera.project(crossHairTmp);
        return project;
    }


    public void handleCrossHairScreenLimit(){
        if (Gdx.input.isCursorCatched()) {
            if (Gdx.input.getX() > Gdx.graphics.getWidth() - 8) {
                Gdx.input.setCursorPosition(Gdx.graphics.getWidth() - 8, Gdx.input.getY());
            }

            if (Gdx.input.getX() < 0) {
                Gdx.input.setCursorPosition(0, Gdx.input.getY());
            }


            if (Gdx.input.getY() > Gdx.graphics.getHeight()) {
                Gdx.input.setCursorPosition(Gdx.input.getX(), Gdx.graphics.getHeight());
            }

            if (Gdx.input.getY() < 8) {
                Gdx.input.setCursorPosition(Gdx.input.getX(), 8);
            }
        }
    }
}
