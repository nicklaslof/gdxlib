package st.rhapsody.gdxlib.render;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.Pool;
import st.rhapsody.gdxlib.debug.DebugPrint;

public class Render {

    private int renderCounter;

    public enum ZLayer {
        SCREENBACKGROUND_0,
        BACKGROUND_1,
        FOREGROUND_OBSTACLE_2,
        FOREGROUND_ENTITY_3,
        PARTICLE_4,
        SKY_5
    }

    private ArrayMap<ZLayer, Array<Drawable>> renderables = new ArrayMap<ZLayer, Array<Drawable>>();

    private static Render instance = new Render();

    public Pool<SpriteDrawable> spriteDrawablePool = new Pool<SpriteDrawable>(1000,100000) {
        @Override
        protected SpriteDrawable newObject() {
            return new SpriteDrawable();
        }

        @Override
        public void free(SpriteDrawable object) {
            object.dispose();
            super.free(object);
        }
    };

    private Render() {
        for (ZLayer ZLayer : ZLayer.values()) {
            renderables.put(ZLayer, new Array<Drawable>());
        }
    }

    public void reset(){
        DebugPrint.print("Render:", "Current renderables :"+renderCounter, 300);
        for (ObjectMap.Entry<ZLayer, Array<Drawable>> renderable : renderables) {
            renderable.value.clear();
        }
        renderCounter = 0;
    }

    public void drawAtZLayer(ZLayer zLayer, SpriteDrawable spriteDrawable){
        renderables.get(zLayer).add(spriteDrawable);
    }

    public void drawAtZLayer(ZLayer zLayer, TextDrawable textDrawable){
        renderables.get(zLayer).add(textDrawable);
    }

    public static Render getInstance() {
        return instance;
    }

    public void render(SpriteBatch spriteBatch){
        for (ObjectMap.Entry<ZLayer, Array<Drawable>> renderable : renderables) {
            Array<Drawable> sprites = renderable.value;
            for (Drawable drawable : sprites) {

                drawable.draw(spriteBatch);

                renderCounter++;
            }
        }
    }

    public interface Drawable{

        void draw(SpriteBatch spriteBatch);
    }

    public static class TextDrawable implements Drawable{
        private String text;
        private Vector2 position = new Vector2();
        private float scale;
        private Color tintColor;
        private BitmapFont font;

        public TextDrawable withText(String text){
            this.text = text;
            return this;
        }

        public TextDrawable onPosition(Vector2 position){
            this.position.set(position);
            return this;
        }

        public TextDrawable withScale(float scale){
            this.scale = scale;
            return this;
        }

        public TextDrawable withFont(BitmapFont font){
            this.font = font;
            return this;
        }

        public TextDrawable withTintColor(Color color){
            this.tintColor = color;
            return this;
        }

        @Override
        public void draw(SpriteBatch spriteBatch) {
            this.font.getData().setScale(this.scale);
            this.font.setColor(this.tintColor);
            this.font.draw(spriteBatch,text,position.x, position.y);
        }
    }

    public class SpriteDrawable implements Drawable{
        private Sprite sprite;
        private Vector2 position = new Vector2();
        private float scale;
        private Color color = new Color();
        private float rotation;
        private boolean flippedLeft = false;
        private boolean upSideDown = false;

        public SpriteDrawable setSprite(Sprite sprite) {
            this.sprite = sprite;
            return this;
        }

        public SpriteDrawable setPosition(float posX, float posY) {
            this.position.set(posX, posY);
            return this;
        }

        public SpriteDrawable setScale(float scale) {
            this.scale = scale;
            return this;
        }

        public SpriteDrawable setColor(Color color) {
            this.color.set(color);
            return this;
        }

        public SpriteDrawable setRotation(float rotation) {
            this.rotation = rotation;
            return this;
        }

        public SpriteDrawable setFlippedLeft(boolean flippedLeft){
            this.flippedLeft = flippedLeft;
            return this;
        }

        public SpriteDrawable setUpSideDown(boolean upSideDown) {
            this.upSideDown = upSideDown;
            return this;
        }

        public Sprite getSprite() {
            return sprite;
        }

        public Vector2 getPosition() {
            return position;
        }

        public float getScale() {
            return scale;
        }

        public Color getColor() {
            return color;
        }

        public float getRotation() {
            return rotation;
        }

        public boolean isFlippedLeft() {
            return flippedLeft;
        }

        public boolean isUpSideDown() {
            return upSideDown;
        }

        @Override
        public void draw(SpriteBatch spriteBatch) {
            Sprite sprite = getSprite();

            if (sprite != null) {
                sprite.setScale(getScale());
                sprite.setColor(getColor());
                sprite.setFlip(isFlippedLeft(),isUpSideDown());
                sprite.setRotation(getRotation());
                sprite.setPosition(getPosition().x, getPosition().y);

                sprite.draw(spriteBatch);
            }
            spriteDrawablePool.free(this);
        }

        public void dispose() {
            this.sprite = null;
            this.position.setZero();
            this.scale = 1.0f;
            this.color.set(Color.WHITE);
            this.rotation = 0.0f;
            this.flippedLeft = false;
            this.upSideDown = false;
        }
    }
}
