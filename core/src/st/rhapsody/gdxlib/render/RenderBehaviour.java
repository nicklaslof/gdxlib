package st.rhapsody.gdxlib.render;

import com.badlogic.gdx.utils.Array;
import st.rhapsody.gdxlib.gameobject.GameObject;
import st.rhapsody.gdxlib.utils.Counter;

public class RenderBehaviour {

    public void tick(GameObject gameObject, float delta){

    }

    public void render(GameObject gameObject, Render render){
        if (render == null){
            return;
        }
        Array<GameObject.GameObjectSprite> entitySprites = gameObject.getEntitySprites();
        for (GameObject.GameObjectSprite gameObjectSprite : entitySprites) {
            if (gameObjectSprite != null) {
                render.drawAtZLayer(gameObjectSprite.getZLayer(),
                        render.spriteDrawablePool.obtain()
                                .setSprite(gameObjectSprite.getSprite())
                                .setFlippedLeft(gameObject.isFlippedLeft())
                                .setUpSideDown(gameObject.isUpSideDown())
                                .setScale(gameObject.getScale())
                                .setColor(gameObject.getTintColor())
                                .setRotation(gameObject.getRotation())
                                .setPosition(gameObject.getPosition().x, gameObject.getPosition().y)
                );
            }

        }
    }

    public void dispose() {
        Counter.getInstance().disposeObject(this);
    }
}
