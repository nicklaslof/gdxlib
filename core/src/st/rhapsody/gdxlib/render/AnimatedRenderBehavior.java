package st.rhapsody.gdxlib.render;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.utils.Array;
import st.rhapsody.gdxlib.debug.DebugPrint;
import st.rhapsody.gdxlib.gameobject.GameObject;
import st.rhapsody.gdxlib.resource.LibSprite;
import st.rhapsody.gdxlib.utils.Counter;

public class AnimatedRenderBehavior extends RenderBehaviour{
    private Animation<GameObject.GameObjectSprite>[] animations = new Animation[32];
    private int currentStateId;
    private GameObject.GameObjectSprite keyFrame;

    public void setCurrentStateId(int id){
        if (this.currentStateId != id){
            Counter.getInstance().setCounter(this, "timer",0);
        }
        this.currentStateId = id;
    }

    public RenderBehaviour addAnimation(GameObjectAnimation gameObjectAnimation){
        int id = gameObjectAnimation.getId();
        Animation<GameObject.GameObjectSprite> animation = new Animation<GameObject.GameObjectSprite>(gameObjectAnimation.delay, gameObjectAnimation.sprites);
        if (!gameObjectAnimation.playOnce) {
            animation.setPlayMode(Animation.PlayMode.LOOP);
        }else{
            animation.setPlayMode(Animation.PlayMode.NORMAL);
        }
        animations[id] = animation;

        return this;
    }

    @Override
    public void tick(GameObject gameObject, float delta) {
        super.tick(gameObject, delta);

        Counter.getInstance().increaseCounter(this, "timer", delta);
        Animation<GameObject.GameObjectSprite> animation = animations[currentStateId];

        float timer = Counter.getInstance().getCounter(this, "timer");
        this.keyFrame = animation.getKeyFrame(timer);
    }

    @Override
    public void render(GameObject gameObject, Render render) {

        gameObject.getEntitySprites().clear();
        gameObject.getEntitySprites().add(this.keyFrame);

        super.render(gameObject, render);
    }

    public static class GameObjectAnimation{
        private int id;
        private Array<GameObject.GameObjectSprite> sprites = new Array<GameObject.GameObjectSprite>();
        private float delay;
        private boolean playOnce;


        public GameObjectAnimation withId(int id){
            this.id = id;
            return this;
        }

        public GameObjectAnimation addSprite(GameObject.GameObjectSprite sprite){
            this.sprites.add(sprite);
            return this;
        }

        public GameObjectAnimation withDelay(float delay){
            this.delay = delay;
            return this;
        }

        public GameObjectAnimation playOnce(){
            this.playOnce = true;
            return this;
        }

        public int getId() {
            return id;
        }

        public Array<GameObject.GameObjectSprite> getSprites() {
            return sprites;
        }

        public float getDelay() {
            return delay;
        }

        public GameObjectAnimation addGameObjectSprite(String spriteName, Render.ZLayer zLayer) {
            if (LibSprite.getInstance().getSprite(spriteName) == null){
                DebugPrint.print(this, "No sprite with name "+spriteName+" found", 0);
            }
            addSprite(new GameObject.GameObjectSprite(LibSprite.getInstance().getSprite(spriteName), zLayer));

            return this;
        }
    }
}
