package st.rhapsody.gdxlib.resource;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.ObjectMap;

public class LibSprite {
    private static LibSprite instance = new LibSprite();
    private ArrayMap<String, Sprite> sprites = new ArrayMap<String, Sprite>();

    public static LibSprite getInstance() {
        return instance;
    }


    public Sprite getSprite(String name){
        if (!sprites.containsKey(name)){

            ArrayMap<Texture, TextureAtlas> atlases = LibAtlas.getInstance().getAtlases();

            atlasloop:
                for (ObjectMap.Entry<Texture, TextureAtlas> atlas : atlases) {
                    Sprite sprite = atlas.value.createSprite(name);
                    if (sprite != null){
                        sprites.put(name, sprite);
                        break atlasloop;
                    }
                }

        }

        return sprites.get(name);
    }


}
