package st.rhapsody.gdxlib.resource;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.ArrayMap;

public class LibAtlas {
    private static final LibAtlas instance = new LibAtlas();
    private ArrayMap<Texture, TextureAtlas> atlases = new ArrayMap<Texture, TextureAtlas>();


    public static LibAtlas getInstance(){
        return instance;
    }

    public void addAtlasRegion(Texture texture, String name, int xLocation, int yLocation, int width, int height){
        if (!atlases.containsKey(texture)){
            TextureAtlas textureAtlas = new TextureAtlas();
            atlases.put(texture, textureAtlas);
        }

        TextureAtlas textureAtlas = atlases.get(texture);

        textureAtlas.addRegion(name, texture, xLocation, yLocation, width, height);
    }

    ArrayMap<Texture, TextureAtlas> getAtlases() {
        return atlases;
    }
}
