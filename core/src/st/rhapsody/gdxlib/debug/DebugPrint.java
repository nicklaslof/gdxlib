package st.rhapsody.gdxlib.debug;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.ArrayMap;

public class DebugPrint {

    private static ArrayMap<Object, Integer> debug = new ArrayMap<Object, Integer>();

    /**
     * Debug print but only when the limit has been reached to avoid spamming 60 messages/second (every tick)
     * @param clazz
     * @param object
     * @param limit
     */
    public static void print(Object clazz, Object object, int limit){

        if (limit == 0){
            doPrint(clazz, object);
            return;
        }
        if (debug.containsKey(clazz)){
            Integer integer = debug.get(clazz);
            if (integer >= limit){
                doPrint(clazz, object);
                debug.put(clazz, 0);
            }else{
                debug.put(clazz, integer+1);
            }
        }else{
            debug.put(clazz, 0);
        }


    }

    private static void doPrint(Object clazz, Object object) {
        String name;
        if (clazz instanceof String){
            name = (String) clazz;
        }else{
            name = clazz.getClass().getCanonicalName();
        }
        Gdx.app.debug(name, object.toString());
    }
}
